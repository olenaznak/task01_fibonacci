package fibonacci;

import exceptions.AutoclosableException;

import java.util.ArrayList;
import java.util.List;

public class FirstTask implements AutoCloseable {
    private final double PERCENTAGE = 100.0;
    private long intervalStart;
    private long intervalEnd;

    public FirstTask(long start, long end) {
        this.intervalStart = start;
        this.intervalEnd = end;
    }

    public long getIntervalStart() {
        return intervalStart;
    }

    public void setIntervalStart(final long intervalStart) {
        this.intervalStart = intervalStart;
    }

    public long getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(final long intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    private boolean isOdd(final long num) {
        if ((num & 1) == 1) {
            return true;
        }
        return false;
    }

    private boolean isEven(final long num) {
        if ((num & 1) == 0) {
            return true;
        }
        return false;
    }

    public void printNumbers() {
        System.out.print("Odd numbers:  ");
        if (isEven(intervalStart)) {
            printOddNumbers(intervalStart + 1);
        } else {
            printOddNumbers(intervalStart);
        }
        System.out.println();
        System.out.print("Even numbers  ");
        if (isEven(intervalEnd)) {
            printEvenNumbers(intervalEnd);
        } else {
            printEvenNumbers(intervalEnd - 1);
        }
        System.out.println();
    }

    private void printOddNumbers(final long start) {
        for (long i = start; i <= intervalEnd; i += 2) {
            System.out.print(i + "  ");
        }
    }

    private void printEvenNumbers(final long end) {
        for (long i = end; i >= intervalStart; i -= 2) {
            System.out.print(i + "  ");
        }
    }

    public void printSumOfNumbers() {
        if (isOdd(intervalStart)) {
            System.out.println("Sum of odd numbers: "
                    + getSum(intervalStart));
            System.out.println("Sum of even numbers: "
                    + getSum(intervalStart + 1));
        } else {
            System.out.println("Sum of odd numbers: "
                    + getSum(intervalStart + 1));
            System.out.print("Sum of even numbers:  "
                    + getSum(intervalStart));
        }
    }

    private int getSum(final long start) {
        int sum = 0;
        for (long i = start; i <= intervalEnd; i += 2) {
            sum += i;
        }
        return sum;
    }

    public void printFibonacci(final List<Long> fibonacciRow) {
        System.out.println("Fibonacci row:");
        for (long i : fibonacciRow) {
            System.out.print(i + "  ");
        }
        System.out.println();
    }

    public List<Long> createFibonacciRow(final int n) {
        long f1;
        long f2;
        if (isOdd(intervalEnd)) {
            f1 = intervalEnd;
            f2 = intervalEnd - 1;
        } else {
            f1 = intervalEnd - 1;
            f2 = intervalEnd;
        }
        List<Long> fibonacciRow = fillFibonacciRow(n, f1, f2);
        return fibonacciRow;
    }

    private List<Long> fillFibonacciRow(final int n, long f1, long f2) {
        int i;
        List<Long> fibonacciRow = new ArrayList<Long>();
        if (f1 < f2) {
            fibonacciRow.add(f1);
            fibonacciRow.add(f2);
        } else {
            fibonacciRow.add(f2);
            fibonacciRow.add(f1);
        }
        for (i = fibonacciRow.size(); i < n; i++) {
            f1 = fibonacciRow.get(i - 2);
            f2 = fibonacciRow.get(i - 1);
            fibonacciRow.add(f1 + f2);
        }
        return fibonacciRow;
    }

    public void printPercentages(final List<Long> fibonacciRow) {
        System.out.printf("Percentage of odd numbers - %.3f",
                numberPercentage(countOddNumbers(fibonacciRow), fibonacciRow.size()));
        System.out.println();
        System.out.printf("Percentage of even numbers - %.3f",
                numberPercentage(countEvenNumbers(fibonacciRow), fibonacciRow.size()));
    }

    private double numberPercentage(final int num, final int size) {
        return num * PERCENTAGE / size;
    }

    private int countOddNumbers(final List<Long> fibonacciRow) {
        int countOdd = 0;
        for (long num : fibonacciRow) {
            if (isOdd(num)) {
                countOdd++;
            }
        }
        return countOdd;
    }

    private int countEvenNumbers(final List<Long> fibonacciRow) {
        int countEven = 0;
        for (long num : fibonacciRow) {
            if (isEven(num)) {
                countEven++;
            }
        }
        return countEven;
    }

    public void close() throws AutoclosableException {
        throw new AutoclosableException();
    }
}