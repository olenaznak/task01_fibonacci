package fibonacci;

import exceptions.AutoclosableException;
import exceptions.IncorrectAmountException;
import exceptions.IncorrectRangeException;
import exceptions.InvalidConfirmationException;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n;
        long start;
        long end;
        boolean exitMarker = false;
        while (true) {
            System.out.println("Start and end of the interval:");
            try {
                start = getLong();
                end = getLong();
                if (start >= end) {
                    throw new IncorrectRangeException();
                }
            } catch (IncorrectRangeException exc) {
                exc.printMessage();
                continue;
            }
            try (FirstTask firstTask = new FirstTask(start, end)) {
                firstTask.printNumbers();
                firstTask.printSumOfNumbers();
                System.out.println("\nAmount of numbers in Fibonacci row:");
                n = getInt();
                try {
                    if (n < 2) {
                        throw new IncorrectAmountException();
                    }
                } catch (IncorrectAmountException exc) {
                    exc.printMessage();
                    break;
                }
                List<Long> fibonacci = firstTask.createFibonacciRow(n);
                firstTask.printFibonacci(fibonacci);
                firstTask.printPercentages(fibonacci);
            } catch (AutoclosableException exc) {
                exc.printMessage();
            }
            System.out.println("\nWould you like to try one more time? (yes/no)");
            while (true) {
                try {
                    if (!getConfirmation()) {
                        exitMarker = true;
                        break;
                    }else {
                        break;
                    }
                } catch (InvalidConfirmationException exc) {
                    exc.printMessage();
                }
            }
            if (exitMarker) {
                break;
            }
        }
    }

    private static long getLong() {
        long num;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLong()) {
            in.next();
        }
        num = in.nextLong();
        return num;
    }

    private static int getInt() {
        int num;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLong()) {
            in.next();
        }
        num = in.nextInt();
        return num;
    }

    private static String getString() {
        String str;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLine()) {
            in.next();
        }
        str = in.nextLine();
        return str;
    }

    private static boolean getConfirmation() throws InvalidConfirmationException {
        String confirm = getString();
        if (confirm.equals("yes")) {
            return true;
        }
        if (confirm.equals("no")) {
            return false;
        }
        System.out.println("Incorrect input, try one more time");
        throw new InvalidConfirmationException();
    }
}
