package exceptions;

public class IncorrectAmountException extends Exception {
    public final void printMessage() {
        System.out.println("Incorrect input of amount " +
                "of numbers in Fibonacci row");
    }
}
