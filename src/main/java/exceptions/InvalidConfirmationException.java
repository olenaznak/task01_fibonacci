package exceptions;

public class InvalidConfirmationException extends Exception {
    public final void printMessage() {
        System.out.println("Invalid confirmation");
    }
}
