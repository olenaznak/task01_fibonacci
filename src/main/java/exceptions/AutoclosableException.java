package exceptions;

public class AutoclosableException extends Exception {
    public final void printMessage() {
        System.out.println("\n\tException in close()\n");
    }
}
